import router from "./src/router/router.js"
import C_header from "./src/components/common/header/header.vue.js"
import { appStore, webApi } from "./src/common/store.js"
import { TOAST_DEFAULT } from "./src/common/constants.js"

const App = Vue.createApp({
    components: {
        C_header,
    },
    template: /*html*/`
    <C_header></C_header>
    <RouterView></RouterView>
    `
})

App.provide('WebApi', webApi)
App.provide('AppStore', appStore)
App.use(router)
App.use(VueToast.ToastPlugin, TOAST_DEFAULT)
App.mount("#vue-div")