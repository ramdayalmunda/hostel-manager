import { clearLocalStore } from "../../common/local-store.js";

const { inject, onMounted } = Vue;
const { useRouter } = VueRouter;
export default {
    setup() {
        let appStore = inject("AppStore")
        let toast = inject("$toast")
        let router = useRouter()
        onMounted(() => {
            setTimeout(()=>{
                appStore.loggedIn = false
                appStore.user = null
                clearLocalStore('user')
                toast.success("Logged out successfully.")
                router.push({ name: "Login" })
            },1000)
        })
    },
    template: /*html*/`
    <h1>Logging Out</h1>
    `
}