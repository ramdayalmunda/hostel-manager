import template from "./header.template.js"

const { useRoute } = VueRouter;
const { reactive, inject } = Vue;
export default {
    template,
    setup(){
        let route = reactive(useRoute())
        let appStore = inject('AppStore')

        return {
            route,
            appStore,
        }
    }
}