export default /*html*/`
<p>
    <span>Header Component</span> &nbsp;
    <RouterLink :to="{name:'Login'}" v-if="!appStore?.loggedIn">Login</RouterLink> &nbsp;
    <RouterLink :to="{name:'User.List'}">Users</RouterLink> &nbsp;
    <RouterLink :to="{name:'Establishment.List'}">Establishments</RouterLink> &nbsp;
    <RouterLink :to="{name:'Tenet.List'}">Tenets</RouterLink> &nbsp;
    <RouterLink :to="{name:'Logout'}" v-if="appStore?.loggedIn">Log out</RouterLink> &nbsp;
</p>
`