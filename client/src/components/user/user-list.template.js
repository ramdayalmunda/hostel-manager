export default /*html*/`
<h1>Managers</h1>
<table>
    <thead>
        <tr>
            <th>Sl.no</th>
            <th>Full Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Shivam</td>
            <td>8596541524</td>
            <td>shivam.agarval@gmail.com</td>
            <td>
                <button><RouterLink :to="{name:'User'}">View</RouterLink></button>
                <button><RouterLink :to="{name:'User'}">Edit</RouterLink></button>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>Rajeev Anand</td>
            <td>8754875487</td>
            <td>anand.rajeev@gmail.com</td>
            <td>
                <button><RouterLink :to="{name:'User'}">View</RouterLink></button>
                <button><RouterLink :to="{name:'User'}">Edit</RouterLink></button>
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td>Shrishti Priya</td>
            <td>8989685895</td>
            <td>shrishti.priva@gmail.com</td>
            <td>
                <button><RouterLink :to="{name:'User'}">View</RouterLink></button>
                <button><RouterLink :to="{name:'User'}">Edit</RouterLink></button>
            </td>
        </tr>
    </tbody>
</table>
`