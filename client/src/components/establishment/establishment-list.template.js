export default /*html*/`

<h1>Establishment List</h1>
<table>
    <thead>
        <tr>
            <th>Sl.no</th>
            <th>Name</th>
            <th>Address</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Anand Vihar</td>
            <td>Alpha 2, H-22, 1st cross street</td>
            <td>
                <button><RouterLink :to="{name:'Establishment'}">View</RouterLink></button>
                <button><RouterLink :to="{name:'Establishment'}">Edit</RouterLink></button>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>Surat Naaj</td>
            <td>Omega 1, B-3 3rd Avenue</td>
            <td>
                <button><RouterLink :to="{name:'Establishment'}">View</RouterLink></button>
                <button><RouterLink :to="{name:'Establishment'}">Edit</RouterLink></button>
            </td>
        </tr>
    </tbody>
</table>

`