export default /*html*/`
<h1>List Of Tenets</h1>
<table>
    <thead>
        <tr>
            <th>Sl.no</th>
            <th>Full Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Ramdayal Munda</td>
            <td>9784845612</td>
            <td>r.dayal.munda@gmail.com</td>
            <td>
                <button><RouterLink :to="{name: 'Tenet'}">View</RouterLink></button>
                <button><RouterLink :to="{name: 'Tenet'}">Edit</RouterLink></button>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>Ria Sharma</td>
            <td>8461348678</td>
            <td>ria.sharma.1999.02@gmail.com</td>
            <td>
                <button><RouterLink :to="{name: 'Tenet'}">View</RouterLink></button>
                <button><RouterLink :to="{name: 'Tenet'}">Edit</RouterLink></button>
            </td>
        </tr>
    </tbody>
</table>
`