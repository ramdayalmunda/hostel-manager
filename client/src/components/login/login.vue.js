import { setLocalData } from "../../common/local-store.js";
import template from "./login.template.js";


const { inject, reactive } = Vue;
const { useRouter } = VueRouter;
export default {
    template,
    setup(){
        let router = useRouter()
        let toast = inject('$toast')
        let webApi = inject('WebApi');
        let appStore = inject('AppStore')
        let userCreds = reactive({ emailOrPhone: null, password: null })
        async function login(){
            await webApi.post("/common/login", userCreds).then((response)=>{
                if ( response?.data?.success ){
                    toast.success(response.data.message)
                    setLocalData(response.data.storeData, 'user')
                    router.push({name: 'Establishment.List'})
                    appStore.loggedIn = true
                    appStore.user = response.data.storeData
                }else {
                    toast.warning(response.data.message)
                }

            }).catch( (error)=>{ toast.error('Some Error Occured'); console.log(error) } )
        }

        return {
            userCreds,

            login,
        }
    }
}