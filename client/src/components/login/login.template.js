export default /*html*/`

<form @submit.prevent="login">
    <h1>Login Page</h1>
    <input v-model="userCreds.emailOrPhone" type="text" placeholder="email or phone" name="email-or-phone">
    <input v-model="userCreds.password  " type="password" placeholder="password" name="password">
    <button type="submit">Submit</button>
</form>

`