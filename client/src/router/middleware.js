import { getLocalStore } from "../common/local-store.js"
import { appStore } from "../common/store.js"
export default {
    reRouteLogin(to, from, next) {
        let userData = getLocalStore('user')
        if (userData?._id) {
            appStore.user = userData
            appStore.loggedIn = true
            next({ name: "Establishment.List" })
            return
        } else {
            appStore.loggedIn = false;
            appStore.user = null;
            next(); // let it route to login
            return
        }
    },
    authRequired(to, from, next) {
        let userData = getLocalStore('user')
        if (userData?._id) {
            appStore.user = userData
            appStore.loggedIn = true
            next()
            return
        } else {
            appStore.loggedIn = false;
            appStore.user = null;
            VueToast.useToast().error("You need to login to visit this page.")
            next({ name: "Login" }); // let it route to login
            return
        }
    }
}