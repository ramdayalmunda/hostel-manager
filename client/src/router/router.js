import { SUB_URL } from "../../env.js"
import middleware from "./middleware.js"

const routes = [
    {
        path: SUB_URL + "/",
        name: "Login",
        beforeEnter: [ middleware.reRouteLogin ],
        component: () => import("../components/login/login.vue.js"),
    },
    {
        path: SUB_URL + "/logout",
        name: "Logout",
        beforeEnter: [],
        component: () => import("../components/common/logout.vue.js"),
    },
    {
        path: SUB_URL + "/establishment-list",
        name: "Establishment.List",
        beforeEnter: [ middleware.authRequired ],
        component: () => import("../components/establishment/establishment-list.vue.js"),
    },
    {
        path: SUB_URL + "/establishment",
        name: "Establishment",
        beforeEnter: [ middleware.authRequired ],
        component: () => import("../components/establishment/establishment.vue.js"),
    },
    {
        path: SUB_URL + "/tenet-list",
        name: "Tenet.List",
        beforeEnter: [ middleware.authRequired ],
        component: () => import("../components/tenet/tenet-list.vue.js"),
    },
    {
        path: SUB_URL + "/tenet",
        name: "Tenet",
        beforeEnter: [ middleware.authRequired ],
        component: () => import("../components/tenet/tenet.vue.js"),
    },
    {
        path: SUB_URL + "/user-list",
        name: "User.List",
        beforeEnter: [ middleware.authRequired ],
        component: () => import("../components/user/user-list.vue.js"),
    },
    {
        path: SUB_URL + "/user",
        name: "User",
        beforeEnter: [ middleware.authRequired ],
        component: () => import("../components/user/user.vue.js"),
    },
]

const router = VueRouter.createRouter({
    routes,
    history: VueRouter.createWebHistory()
})

export default router