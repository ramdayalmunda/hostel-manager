import { API_URL } from "../../env.js";
import axios from "../../assets/js/axios.ejs.min.js" // ejs modules can be imported directly in js file without the script tag
import { getLocalStore } from "./local-store.js";

export const webApi = axios.create({
    baseURL: API_URL
})

let userData = getLocalStore('user')
export const appStore = Vue.reactive({
    loggedIn: userData?._id?true: false,
    user: userData?._id?userData:null
})