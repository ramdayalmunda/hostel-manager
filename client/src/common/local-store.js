import { SUB_URL } from "../../env.js"

export function setLocalData(data, storeName){
    storeName = (SUB_URL?SUB_URL+'.':'')+storeName
    let storeData = localStorage.getItem(storeName)
    let newStoreData = data
    if(storeData){
        storeData = JSON.parse(storeData)
        newStoreData = {
            ...newStoreData,
            ...storeData,
        }
    }
    localStorage.setItem(storeName, JSON.stringify(newStoreData))
    return newStoreData
}

export function getLocalStore(storeName){
    storeName = (SUB_URL?SUB_URL+'.':'')+storeName
    let storeData = localStorage.getItem(storeName)
    if(storeData) storeData = JSON.parse(storeData)
    return storeData;
}

export function clearLocalStore(storeName){
    storeName = (SUB_URL?SUB_URL+'.':'')+storeName
    localStorage.removeItem(storeName)
}