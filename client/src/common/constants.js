export const TOAST_DEFAULT = {
    position: 'top-right',
    // type: 'success',
    duration: 3000,
    dismissible: true, // disable on click?
    queue: false, // wait for existing one to dismiss?
    pauseOnHover: true, // don't dismiss if being hovered?
}