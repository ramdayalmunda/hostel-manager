global.ENV = require("../env.js")
const mongoose = require("mongoose")
const model = require("../model/index.js")
const seedsList = [
    {
        model: model.User,
        name: "User",
        records: [
            {
                name: "vo ji kiq",
                email: "vojykiq@getnada.com",
                phone: "8596859874",
                admin: true,
                password: "$2b$10$0wg6hb/T04YPDrFCRO.8auGxG8qnrIfjwatIeQMIyAmM9diCcKEHe"
            }
        ]
    }
]
const clearSeeds = [
    "User"
]

!async function () {
    console.log('Seeds Started')
    await mongoose.connect(ENV.MDB.url)
        .then(async () => {
            for (const seed of clearSeeds) {
                await model[seed].deleteMany()
                console.log('collection', seed, 'cleared')
            }
            
            for ( const seed of seedsList ){

                await seed.model.insertMany(seed.records)
                console.log(seed.records.length, `records generated for `, seed.name)

            }

        })
        .catch((error) => { console.log('Seeds Failed due to failed MDB connection') })

    console.log('Seeds Completed')
    process.exit()
}()
