const messages = require("../helper/messages.js")
const { STATUS } = require("../helper/constants.js")
const userDao = require("../dao/user.js")

const bcrypt = require('bcrypt')

module.exports.login = async (req, res) => {
    try {
        let userData = null;
        let resultObj = { success: false, message: messages.NO_USER }
        userData = await userDao.getUserWithEmai(req.body.emailOrPhone)
        if (!userData) userData = await userDao.getUserWithPhone(req.body.emailOrPhone)
        if (userData) {

            if (req.body.password && await bcrypt.compare(req.body.password, userData.password)) {
                resultObj.success = true;
                resultObj.message = messages.LOGIN_SUCCESS
                resultObj.storeData = {
                    _id: userData._id,
                    name: userData.name,
                    email: userData.email,
                    phone: userData.phone,
                    admin: userData.admin,
                }
            }else resultObj.message = messages.INVALID_CREDENTIALS
            // let newPassword = await bcrypt.hash( "QW!@qw12",ENV.ENCRYPTION.saltRound )

        }
        res.status(STATUS.OK).json(resultObj)
    } catch (err) {
        console.log(err)
        res.status(STATUS.OK).json({ message: messages.INTERNAL_SERVER_ERROR })
    }
}