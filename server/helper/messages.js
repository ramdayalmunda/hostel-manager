module.exports = {
    OK: "Success",
    INTERNAL_SERVER_ERROR: "Internal Server Error",
    LOGIN_SUCCESS: "Logged In Successfully",
    INVALID_CREDENTIALS: "Invalid Credentials",
    NO_USER: "User not found",
}