const model = require("../model/index.js")

module.exports.getUserWithEmai = async function(email){
    return await model.User.findOne( { email: email } ).lean()
}

module.exports.getUserWithPhone = async function( phone ){
    return await model.User.findOne( { phone: phone } ).lean()
}