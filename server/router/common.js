const express = require('express')
const commonController = require("../controller/common.js")
const router = express.Router()

router.post( "/login", commonController.login )


module.exports = router