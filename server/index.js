global.ENV = require('./env.js')
const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const app = express()

app.use( bodyParser.json() )

app.use('/common', require('./router/common.js'))

app.listen( ENV.PORT, ()=>{
    console.log(`Express Server on http://localhost:${ENV.PORT}`)
    mongoose.connect( ENV.MDB.url )
        .then( ()=>{ console.log('MDB connected') } )
        .catch( (error)=>{ console.log('Failed MDB connection', error) } )
} )