const mongoose = require('mongoose')
const User = require('./user.js')

const model = {
    [User.collectionName]: new mongoose.model(User.collectionName, User.collectionSchema)
}

module.exports = model