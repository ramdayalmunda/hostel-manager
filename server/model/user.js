const mongoose = require('mongoose');

const collectionSchema = new mongoose.Schema({
    name: String,
    email: String,
    phone: String,
    admin: { type: Boolean, default: false },
    password: String
}, { timestamps: true })

const collectionName = "User"

module.exports = { collectionName, collectionSchema }